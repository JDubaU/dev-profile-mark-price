//
//  BorderButton.swift
//  Dev-Profile-Mark-Price
//
//  Created by Jake Wojtas on 1/29/18.
//  Copyright © 2018 Jake Wojtas. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        self .awakeFromNib()
        layer.cornerRadius = 3.0
        
    }

}
