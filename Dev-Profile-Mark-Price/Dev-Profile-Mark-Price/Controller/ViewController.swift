//
//  ViewController.swift
//  Dev-Profile-Mark-Price
//
//  Created by Jake Wojtas on 1/28/18.
//  Copyright © 2018 Jake Wojtas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var devLogo: UIImageView!
    @IBOutlet weak var customizeDashboardBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        devLogo.layer.cornerRadius = 5.0
        devLogo.clipsToBounds = true
        customizeDashboardBtn.layer.cornerRadius = 5.0
        customizeDashboardBtn.clipsToBounds = true
        
    }


    
    
    
    


}

